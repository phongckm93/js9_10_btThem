function getID(a) {
  return document.getElementById(a);
}

//BÀI 1:
getID("bai_1").onclick = function In() {
  var chuoi = "";
  for (var i = 0; i < 100; i += 10) {
    chuoi = document.createElement("tr");
    for (var index = i + 1; index <= i + 10; index++) {
      a = createTd(index);
      chuoi.appendChild(a);
      getID("ketQua1").appendChild(chuoi);
    }
  }
};
function createTd(i) {
  var node = document.createElement("td");
  var textnode = document.createTextNode(i);
  node.appendChild(textnode);
  return node;
}

//Bài 2:
var mang = [];
getID("add").onclick = function mangSo() {
  var n = getID("soN").value * 1;
  mang.push(n);
  getID("mang").innerHTML = mang;
  getID("soN").value = "";
};
function ktSoNguyenTo(n) {
  if (n <= 1) {
    return false;
  } else {
    var k = Math.sqrt(n);
    for (var i = 2; i <= k; i++) {
      if (n % i == 0) {
        return false;
      }
    }
    return true;
  }
}
getID("inMang").onclick = function checkMang() {
  var b = [];
  for (var i = 0; i < mang.length; i++) {
    var a = mang[i];
    if (ktSoNguyenTo(a) == true) {
      b.push(a);
    }
  }
  if (b.length == 0) {
    getID("ketQua2").innerHTML = `không có số nguyên tố nào`;
  } else {
    getID("ketQua2").innerHTML = b;
  }
};
//Bài 3:
getID("tinhTong").onclick = function tinhTong() {
  var a = getID("soN_3").value * 1;
  var sum = (a * (a + 1)) / 2 + 2 * a - 1;
  getID("ketQua3").innerHTML = sum;
};
//Bài 4:
getID("inUoc").onclick = function inUoc() {
  var a = "";
  var b = 0;
  var n = getID("soN_4").value * 1;
  if (n == 0 || n == "") {
    alert("Xin kiểm tra dữ liệu nhập");
    return;
  } else {
    for (var i = 1; i <= n; i++) {
      if (n % i == 0) {
        a += i + "  ";
        b++;
      }
    }
    getID("ketQua4").innerHTML = `Số ${n} có ${b} ước là ${a}`;
  }
};

//Bài 5:
function xuLi(n) {
  var b = "";
  for (var i = n.length; i >= 0; i = i - 1) {
    var k = n.charAt(i);
    b += k;
  }
  return b;
}
getID("daoSo").onclick = function bai5() {
  var n = getID("soN5").value;
  if (n == "" || n <= 0) {
    alert("Xin kiểm tra dữ liệu nhập");
  } else {
    getID("ketQua5").innerHTML = xuLi(n);
  }
};
//Bài 6:
getID("bai_6").onclick = function tinh() {
  var sum = 0;

  for (var i = 1; sum <= 100; i++) {
    sum += i;
    var k = i;
  }
  getID("ketQua6").innerHTML = k - 1;
};
//Bài 7:
getID("bai_7").onclick = function bai7() {
  var n = getID("soN_7").value * 1;
  var a = "";
  for (var i = 0; i < 11; i++) {
    var b = n * i;
    a += `${n}x${i}=${b}<br>`;
  }
  getID("ketQua7").innerHTML = a;
};
//Bài 8:
var cards = [
  "4K",
  "KH",
  "5C",
  "KA",
  "QH",
  "KD",
  "2H",
  "10S",
  "AS",
  "7H",
  "9K",
  "10D",
];
getID("themBai").onclick = function themBai() {
  var newCard = getID("newCard").value;
  if (newCard != "") {
    cards.push(newCard);
  }
  getID("cards").innerHTML = cards;
  getID("newCard").value = "";
};
getID("bai_8").onclick = function chiaBai() {
  var a = [];
  var b = [];
  var c = [];
  var d = [];
  // getID("ketQua8").value = "";
  for (var i = 0; i < cards.length; i += 4) {
    a.push(cards[i]);
    if (i + 1 >= cards.length) {
      getID(
        "ketQua8"
      ).innerHTML = `player 1: ${a}<br>player 2: ${b}<br>player 3: ${c}<br>player 4: ${d}<br>`;
      return;
    } else {
      b.push(cards[i + 1]);
    }
    if (i + 2 >= cards.length) {
      getID(
        "ketQua8"
      ).innerHTML = `player 1: ${a}<br>player 2: ${b}<br>player 3: ${c}<br>player 4: ${d}<br>`;
      return;
    } else {
      c.push(cards[i + 2]);
    }
    if (i + 3 >= cards.length) {
      getID(
        "ketQua8"
      ).innerHTML = `player 1: ${a}<br>player 2: ${b}<br>player 3: ${c}<br>player 4: ${d}<br>`;
      return;
    } else {
      d.push(cards[i + 3]);
    }
  }
  getID(
    "ketQua8"
  ).innerHTML = `player 1: ${a}<br>player 2: ${b}<br>player 3: ${c}<br>player 4: ${d}<br>`;
};
//Bài 9:
function ktNumber(n) {
  if (n < 0 || Number.isInteger(n) == false) {
    return false;
  } else {
    return true;
  }
}
getID("bai_9").onclick = function gaCho() {
  var m = getID("convat").value * 1;
  var n = getID("chan").value * 1;
  var soGa = 2 * m - n / 2;
  var soCho = n / 2 - m;
  if (ktNumber(soCho) == false || ktNumber(soGa) == false) {
    // alert("Dữ liệu không hợp lệ")
    getID("ketQua9").innerHTML = "Dữ liệu không hợp lệ, xin hãy kiểm tra !";
    return;
  } else {
    getID("ketQua9").innerHTML = `Số gà là ${soGa}<br>Số chó là ${soCho}`;
  }
};

//Bài 10:
getID("time").onclick = function tinhGoc() {
  var kimGio = getID("kimGio").value;
  var kimPhut = getID("kimPhut").value;
  if (
    kimGio === "" ||
    kimPhut === "" ||
    kimGio * 1 > 23 ||
    kimPhut * 1 > 59 ||
    kimGio * 1 < 0 ||
    kimPhut * 1 < 0
  ) {
    alert("kiểm tra giá trị nhập");
    return;
  } else {
    var goc = kimGio * 30 + kimPhut / 2 - kimPhut * 6;
    document.getElementById("ketQua10").innerHTML = `${Math.abs(goc)} độ hoặc ${360 - Math.abs(goc)
      } độ `;
  }
};
